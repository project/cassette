<?php
/**
 * @file
 * Implements theme settings for site admins.
 */

// Include the definition of zen_settings() and zen_theme_get_default_settings().
include_once './' . drupal_get_path('theme', 'zen') . '/theme-settings.php';

/**
 * Implementation of THEMEHOOK_settings().
 */
function STARTERKIT_settings($saved_settings) {
  // Get the default values from the .info file.
  $defaults = zen_theme_get_default_settings('STARTERKIT');
  // Merge the saved variables and their default values.
  $settings = array_merge($defaults, $saved_settings);

  $form = array();
  /* -- Delete this line if you want to use this setting.
  $form['STARTERKIT_example'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use this sample setting'),
    '#default_value' => $settings['STARTERKIT_example'],
    '#description'   => t("This option doesn't do anything; it's just an example."),
  );
  // */

  // Add the base theme's settings.
  $form += zen_settings($saved_settings, $defaults);

  // We don't need to select the base stylesheet.
  unset($form['themedev']['zen_layout']);

  return $form;
}
