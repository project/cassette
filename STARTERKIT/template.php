<?php
/**
 * @file
 * Overrides and preprocess functions for our theme.
 */

/**
 * Implementation of HOOK_theme().
 */
function STARTERKIT_theme(&$existing, $type, $theme, $path) {
  // The following line is needed to get Zen to work properly.
  $hooks = zen_theme($existing, $type, $theme, $path);

  return $hooks;
}
