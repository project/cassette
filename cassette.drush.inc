<?php
/**
 * @file
 * Cassette Drush commands.
 */

/**
 * Implementation of hook_drush_command().
 */
function cassette_drush_command() {
  $items = array();

  $items['cassette-kit'] = array(
    'description' => 'Set up a starter kit.',
    'arguments' => array(
      'name' => 'A name for your theme.',
    ),
    'options' => array(
      'machine_name' => '[a-z, 0-9] A machine-readable name for your theme.',
    ),
    'examples' => array(
      'drush cassette-kit "My theme name"',
    ),
  );

  return $items;
}

/**
 * Command: Set up a starter kit.
 */
function drush_cassette_kit($name = 'My theme') {
  $machine_name = drush_get_option('machine_name');
  $machine_name = $machine_name ? $machine_name : preg_replace('/[^a-z0-9]+/', '', strtolower($name));

  $cassette_path = drush_locate_root() . '/' . drupal_get_path('theme', 'cassette');

  // From Cassette's location, we move up one directory and construct the path
  // where our new theme will be created.
  $_cassette_path = explode('/', $cassette_path);
  array_pop($_cassette_path);
  $theme_path = implode('/', $_cassette_path) . '/' . $machine_name;

  // Copy the starter kit code base.
  drush_op('cassette_copy_recursive', "$cassette_path/STARTERKIT", $theme_path);

  // Replace all occurences of 'STARTERKIT' with the machine name of our theme.
  drush_op('rename', "$theme_path/STARTERKIT.info.txt", "$theme_path/$machine_name.info");
  drush_op('cassette_file_str_replace', "$theme_path/$machine_name.info", '= STARTERKIT', "= $name");
  drush_op('cassette_file_str_replace', "$theme_path/theme-settings.php", 'STARTERKIT', $machine_name);
  drush_op('cassette_file_str_replace', "$theme_path/template.php", 'STARTERKIT', $machine_name);

  // Enforce detection of theme changes by clearing the cache.
  drush_op('drupal_flush_all_caches');

  drush_print(dt('Created starter kit "!name" in !path.',
    array(
      '!name' => $name,
      '!path' => $theme_path,
    )
  ));
}

/**
 * Recursively copy a directory.
 */
function cassette_copy_recursive($source_directory, $target_directory, $ignore = '/^(\.(\.)?|CVS|SVN|\.DS_Store)$/') {
  if (!is_dir($source_directory)) {
    drush_die(dt('Could not find directory "!directory".', array('!directory' => $source_directory)));
  }

  // Get the source and target directories ready.
  $directory = opendir($source_directory);
  @mkdir($target_directory);

  while($file = readdir($directory)) {
    if (!preg_match($ignore, $file)) {
      // We execute this function again when copying a directory, so all files
      // inside the directory will be copied too.
      if (is_dir("$source_directory/$file")) {
        cassette_copy_recursive("$source_directory/$file", "$target_directory/$file", $ignore);
      }
      else {
        copy("$source_directory/$file", "$target_directory/$file");
      }
    }
  }
  closedir($directory);
}

/**
 * Replace strings in a file.
 */
function cassette_file_str_replace($file_path, $find, $replace) {
  $file_contents = file_get_contents($file_path);
  $file_contents = str_replace($find, $replace, $file_contents);
  file_put_contents($file_path, $file_contents);
}
